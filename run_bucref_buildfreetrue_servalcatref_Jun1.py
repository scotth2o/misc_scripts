# A standalone python script that runs the automated model building pipeline for EM
# same as the one in CCPEM package
# Requires CCP4 package to be installed to run (Buccaneer, REFMAC, sftools, freerflag)
# Author: S.W. Hoh, University of York, 2020
# Reference:
# 'Current approaches for automated model building into cryo-EM maps using
# Buccaneer with CCP-EM', Soon Wen Hoh, Tom Burnley and Kevin Cowtan
# Acta Cryst. (2020). D76, 531-541, https://doi.org/10.1107/S2059798320005513


import os
import subprocess
import sys
import datetime
import argparse
import gemmi
import numpy
import pandas as pd

# Build with Buccaneer
def run_buc(
    pdbin,
    mtzfile,
    seqin,
    resolution,
    cycle,
    EMD,
    logfile,
    int_cyc=5,
    cpujobs=2,
    mode="EM",
    keyfile=None,
    filesuffix="",
    build_from_map=False,
):
    """
    Setup and run Buccanner for the cycle
    Arguments:
    pdbin   : PDB file to continue from, set as None if starting without one
    mtzfile : Reflections data file (produced using maptomtz function that runs REFMAC)
    seqin   : Structure sequence file
    resolution : Resolution of the map
    cycle   : Current pipeline cycle count
    EMD     : Just a name of the EM data to set to title
    logfile : Path for stdout logfile
    col_suf : Column suffix, used when sharpening or blurring applied, and S0 for none
    int_cyc : internal Buccaneer cycle, default=5
    cpujobs : default set to 2
    mode    : set to 'EM' to get the EM reference datafile, otherwise xtal
    keyfile : file with keywords, separate each one with newline (optional)
    filesuffix : suffix for output file (optional)

    Returns: Path to Buccaneer output PDB file
    """
    CWD = os.getcwd()

    if not os.path.isfile(seqin):
        print("Sequence file {0} not found.".format(seqin))
        return
    if not os.path.isfile(mtzfile):
        print("mtzfile not found at {0}.".format(mtzfile))
        return
    pdbout = "build{0}{1}.cif".format(str(cycle), str(filesuffix))
    xmlout = "program{0}{1}.xml".format(str(cycle), str(filesuffix))

    if pdbin == None:
        pdbin_line = ""
    else:
        pdbin_line = "pdbin {0}".format(pdbin)

    if keyfile == None:
        keywords_line = ""
    else:
        fopen = open(keyfile, "r")
        keywords_line = ""
        for line in fopen:
            keywords_line += line
    # clibd + em reference path
    if mode == "EM":
        pdbin_ref = os.path.join(
            os.environ["CLIBD"], "reference_structures/reference-EMD-4116.pdb"
        )
        mtzin_ref = os.path.join(
            os.environ["CLIBD"], "reference_structures/reference-EMD-4116.mtz"
        )
    else:
        pdbin_ref = os.path.join(
            os.environ["CLIBD"], "reference_structures/reference-1tqw.pdb"
        )
        mtzin_ref = os.path.join(
            os.environ["CLIBD"], "reference_structures/reference-1tqw.mtz"
        )

    if build_from_map:
        fout = "Fout"
        pout = "Pout"
    else:
        fout = "FWT"
        pout = "PHWT"

    argsin = """
title {0}_piperun
pdbin-ref {1}
mtzin-ref {2}
colin-ref-fo FP.F_sigF.F,FP.F_sigF.sigF
colin-ref-hl FC.ABCD.A,FC.ABCD.B,FC.ABCD.C,FC.ABCD.D
seqin {3}
mtzin {4}
colin-fo {5},SIGF
colin-phifom {6},FOM
colin-free FreeR_flag
cycles {7}
jobs {8}
anisotropy-correction
fast
cif
correlation-mode
resolution {9}
pdbout {10}
xmlout {11}
{12}
{13}
eof
""".format(
        EMD,
        pdbin_ref,
        mtzin_ref,
        seqin,
        mtzfile,
        fout,
        pout,
        int_cyc,
        cpujobs,
        resolution,
        pdbout,
        xmlout,
        pdbin_line,
        keywords_line,
    )

    log = open(logfile, "a")
    log.writelines("cbuccaneer --stdin << eof {0}".format(argsin))
    log.flush()
    proc = subprocess.Popen(
        "cbuccaneer --stdin << eof {0}".format(argsin),
        universal_newlines=True,
        stdout=log,
        shell=True,
    )
    print("Running Buccaneer cycle {0}".format(cycle))
    sys.stdout.flush()
    proc.wait()
    log.flush()
    log.close()
    return pdbout


# Refinement with REFMAC - OLD
def run_ref(
    pdbin,
    bucmtz,
    resolution,
    cycle,
    logfile,
    sharpfact,
    ref_cyc=20,
    mode="Global",
    libgref=None,
):
    """
    Setup and runs refinement with REFMAC in between Buccanner runs
    Arguments:
    pdbin   : PDB input
    bucmtz  : Initial reflections data file (in CCPEM pipeline, the initial reflections calculated from original map is used)
    Resolution : Resolution of the map
    cycle   : Current pipeline cycle count
    logfile : Path of stdout logfile
    sharpfact : sharpening factor >0 blur, <0 sharp, 0 is none
    ref_cyc : Interal REFMAC refinement cycles, default is 20
    mode    : Default is 'Global' for global refinement, 'Local' for local refinement
    libgref : libg reference file produced using ligb

    Returns : Path to refined PDB output
    """

    # local refinement
    if mode == "Local":
        hklin = "masked_fs.mtz"
        shiftline = "@shifts.txt"
    else:  # global refinement (default)
        hklin = bucmtz
        shiftline = ""
    hklout = "refine.mtz"
    xyzout = "refined{0}.pdb".format(str(cycle))
    if sharpfact >= 0:
        if sharpfact == 0:
            ref_line = ""
        else:
            ref_line = "refine sharp {0:.2f}".format(-1 * sharpfact)
    else:
        ref_line = "refine sharp {0:.2f}".format(-1 * sharpfact)

    if libgref != None:
        libgresline = "@{0}".format(libgref)
    else:
        libgresline = ""
    argsin = """
make hydr no
solvent no
source EM MB
ncycle {0}
weight auto
reso {1}
ridge dist sigma 0.01
ridge dist dmax 4.2
labin PHIB=Pout0 FP=Fout0
PHOUT
{2}
{3}
{4}
end
eof
""".format(
        ref_cyc, resolution, ref_line, shiftline, libgresline
    )

    log = open(logfile, "a")
    log.writelines("\n----------------------\n")
    log.writelines(
        "refmac5 xyzin {0} hklin {1} xyzout {2} hklout {3} << eof {4}".format(
            pdbin, hklin, xyzout, hklout, argsin
        )
    )
    log.flush()
    proc = subprocess.Popen(
        "refmac5 xyzin {0} hklin {1} xyzout {2} hklout {3} << eof {4}".format(
            pdbin, hklin, xyzout, hklout, argsin
        ),
        universal_newlines=True,
        stdout=log,
        shell=True,
    )
    if mode == "Local":
        print("Running Refmac local refine cycle {0}".format(cycle))
    else:
        print("Running Refmac cycle {0}".format(cycle))
    sys.stdout.flush()
    proc.wait()
    log.flush()
    log.close()
    return xyzout


# refine with Servalcat-REFMAC - New
def run_servalcat(
    pdbin,
    resolution,
    ncycles,
    cycle,
    logfile,
    mapin=None,
    hm1=None,
    hm2=None,
    mode="Global",
    mask=None,
    no_shift=True,
    no_trim=True,
    mask_radius=3.0,
    symmetry_auto=None,
    set_bfactor=True,
    atom_bfactor=40.0,
    weight_auto=True,
    weight=0.0001,
    libg_restraints=None,
    jellybody_on=True,
    jellybody_sigma=0.01,
    jellybody_dmax=4.2,
    output_prefix="refined",
):
    cwd = os.getcwd()
    if hm1 is None:
        if mapin is None:
            print("Please supply map")
            exit()
            # return RefmacResult(structure=None, fscaverage=0.0, weight=0.0,)
        argsin = f"--model {pdbin} --map {mapin}"
        argsin += f" --resolution {resolution} --ncycle {ncycles}"
        if not os.path.exists(mapin):
            print(f"{mapin} does not exist")
            exit()
    else:
        argsin = f"--model {pdbin} --halfmaps {hm1} {hm2}"
        argsin += f" --resolution {resolution} --ncycle {ncycles}"
        if not os.path.exists(hm1):
            print(f"{hm1} does not exist")
            exit()
        # return RefmacResult(structure=None, fscaverage=0.0, weight=0.0,)
        if not os.path.exists(hm2):
            print(f"{hm2} does not exist")
            exit()
            # return RefmacResult(structure=None, fscaverage=0.0, weight=0.0,)

    if not os.path.exists(pdbin):
        print(f"{pdbin} does not exist")
        exit()
        # return RefmacResult(structure=None, fscaverage=0.0, weight=0.0,)
        # return RefmacResult(structure=None, fscaverage=0.0, weight=0.0,)
    if mode == "Global":
        argsin += " --no_mask"
    if mode == "Masked":
        if mask is not None:
            argsin += f" --mask {mask}"
        else:
            argsin += f" --mask_radius {mask_radius}"
    if no_shift:
        argsin += " --no_shift"
    if no_trim:
        argsin += " --no_trim"

    if set_bfactor and atom_bfactor is not None:
        argsin += f" --bfactor {atom_bfactor}"
    if jellybody_on:
        argsin += f" --jellybody --jellybody_params {jellybody_sigma}"
        argsin += f" {jellybody_dmax}"
    argsin += f" --output_prefix {output_prefix}"
    if weight is not None and not weight_auto:
        argsin += f" --weight_auto_scale {weight}"

    # servalcatlog = f"{cwd}/servalcat.log"
    result = {}
    """if os.path.exists(servalcatlog):
        with open(servalcatlog, 'r') as f:
            lines = f.readlines()
            lines = reversed(lines)
            for line in lines:
                if "FSCaverage(full)" in line:
                    result["fscavg"] = line.split('=')[-1].strip()
                if "Weight used" in line:
                    result["weight"] = line.split(":")[-1].strip()
                if "# Finished" in line:
                    skip_refine = True
        if skip_refine:
            return RefmacResult(structure=f"{cwd}/refined.pdb",
                                fscaverage=float(result["fscavg"]),
                                weight=float(result["weight"]),)                   
    """
    log = open(logfile, "a")
    log.writelines("\n----------------------\n")
    log.writelines("servalcat refine_spa {0}".format(argsin))
    log.flush()
    proc = subprocess.Popen(
        "servalcat refine_spa {0}".format(argsin),
        universal_newlines=True,
        stdout=subprocess.DEVNULL,
        shell=True,
    )
    print(f"Running servalcat refine_spa for {pdbin}, cycle {cycle}")
    sys.stdout.flush()
    proc.wait()
    sys.stdout.flush()
    proc.wait()
    log.flush()
    log.close()
    # result = {}
    # filename = f"{cwd}/servalcat.log"
    # if os.path.exists(filename):
    #    with open(filename, 'r') as f:
    #        for line in f:
    #            if "FSCaverage(full)" in line:
    #                result["fscavg"] = line.split('=')[-1].strip()
    #            if "Weight used" in line:
    #                result["weight"] = line.split(":")[-1].strip()
    # if "fscavg" in result:
    #    return RefmacResult(structure=f"{cwd}/refined.pdb",
    #                        fscaverage=float(result["fscavg"]),
    #                        weight=float(result["weight"]),)
    # else:
    #    return RefmacResult(structure=None, fscaverage=0.0, weight=0.0,)

    return f"{output_prefix}.pdb"


# Make mtz file from halfmaps using servalcat
def run_halfmaptomtz(halfmaps: list = [], mask=None, resolution=2.0):
    """
    Use servalcat to create mtz file (nemap.mtz)
    Arguments:
    halfmaps : [halfmap1, halfmap2]
    resolution : map resolution
    textin : sharpening and blurring factor, 0 for none.
             e.g. 0,50,100 will apply sharpening and blurring factor of 50 and 100 respectively.

    Output : mtz file
    """
    hklout = "nemap.mtz"
    # textin = ''
    # if sharpfact >= 0:
    #    if sharpfact == 0:
    #        textin = 'sharp 0'  # none
    #    else:
    #        textin = 'blur 0, {0}'.format(sharpfact)  # blurring
    # else:
    #    textin = 'sharp 0, {0}'.format(-1*sharpfact)  # sharpening

    argsin = "util nemap "
    argsin += f"--halfmaps {halfmaps[0]} {halfmaps[1]} "
    if mask:
        argsin += f"--mask {mask} "
    argsin += f"--resolution {resolution} "
    proc = subprocess.Popen(
        "servalcat {0}".format(argsin),
        universal_newlines=True,
        stdout=subprocess.DEVNULL,
        shell=True,
    )
    print("Running servalcat util nemap for {0} ".format(halfmaps))
    sys.stdout.flush()
    proc.wait()
    sys.stdout.flush()
    print("--Done--")


# make mtz from map
def run_maptomtz(mapin=None, mask=None, resolution=2.0):
    argsin = "sfcalc "
    if mask:
        argsin += f"--map {mapin} --mask {mask} --no_shift --no_trim --resolution {resolution}"
    else:
        argsin += f"--map {mapin} --no_mask --resolution {resolution}"
    proc = subprocess.Popen(
        "servalcat {0}".format(argsin),
        universal_newlines=True,
        stdout=subprocess.DEVNULL,
        shell=True,
    )
    print("Running servalcat sfcalc for {0} ".format(mapin))
    sys.stdout.flush()
    proc.wait()
    sys.stdout.flush()
    print("--Done--")


# sftools
def run_sftools(FOM=1.0, SIGF=0.5, mtzin="nemap.mtz", mtzout="starting_nemap.mtz"):
    """
    Create 'fake' columns SIGF and FOM
    """
    argsin = """
READ {0}
CALC Q COL SIGFem = {1}
CALC W COL FOMem = {2}
WRITE {3}
END
eof
""".format(
        mtzin,
        SIGF,
        FOM,
        mtzout,
    )

    logfile = "sftools_stdout.txt"
    log = open(logfile, "w")
    log.writelines("sftools << eof {0}".format(argsin))
    log.flush()
    proc = subprocess.Popen(
        "sftools << eof {0}".format(argsin),
        universal_newlines=True,
        stdout=log,
        shell=True,
    )
    print("--Running sftools--")
    proc.wait()
    log.flush()
    log.close()
    print("--Done--")

# add sigf and fom with gemmi
def set_sigf_fom_columns(FOM=1.0, SIGF=0.1, mtzin="nemap.mtz", mtzout="starting_nemap.mtz"):
    """
    set column FOM,W to 1.0 and SIGF,Q to 10%(default) of F values
    """
    mtz = gemmi.read_mtz_file(mtzin)
    data_array = numpy.array(mtz, copy=True)
    data_frame = pd.DataFrame(data=data_array, columns=mtz.column_labels())
    mtz.add_column("SIGF","Q")
    has_fwt = mtz.column_with_label("FWT")
    has_fout = mtz.column_with_label("Fout")
    if has_fwt:
        data_frame["SIGF"] = SIGF * data_frame["FWT"]
    if has_fout:
        data_frame["SIGF"] = SIGF * data_frame["Fout"]
    mtz.add_column("FOM","W")
    data_frame["FOM"] = FOM
    mtz.set_data(data_frame.to_numpy())
    mtz.history += ["Added columns SIGF and FOM using GEMMI."]
    mtz.write_to_file(mtzout)

def mapmask(mapin, knl_rad=10, resolution=10):
    """
    input map
    knl_rad: kernal radius in voxels
    resolution: low resolution cutoff
    """
    # emda = "/home/swh514/.local/bin/emda"
    argsin = f"--map {mapin} --knl {knl_rad} --res {resolution}"
    proc = subprocess.Popen(
        "emda mapmask {0}".format(argsin),
        universal_newlines=True,
        stdout=subprocess.DEVNULL,
        shell=True,
    )
    print("Running EMDA to make mask from map")
    sys.stdout.flush()
    proc.wait()
    sys.stdout.flush()
    print("--Done--")


def trim_maps(mapin=None, hm1=None, hm2=None, mask=None, shifts=None, noncubic=False):
    argsin = f"trim --maps "
    if hm1 and hm2:
        argsin += f"{hm1} {hm2} "
    if mapin:
        argsin += f"{mapin} "
    if shifts:
        if mask:
            argsin += f"{mask} --shifts {shifts} "
        else:
            argsin += f"--shifts {shifts} "
    else:
        if mask:
            argsin += f"--mask {mask} "
    if noncubic:
        argsin += f"--noncubic"
    proc = subprocess.Popen(
        f"servalcat {argsin}",
        universal_newlines=True,
        stdout=subprocess.DEVNULL,
        shell=True,
    )
    print("Trim maps with servalcat")
    sys.stdout.flush()
    proc.wait()
    sys.stdout.flush()
    print("--Done--")


def run_freerflag(mtzin="starting_nemap.mtz", mtzout="starting_nemap1.mtz"):
    argsin = """
FREERFRAC 0.05
eof
"""
    logfile = "setfreerflag_stdout.txt"
    log = open(logfile, "w")
    log.writelines(
        "freerflag hklin {0} hklout {1} << eof {2}".format(mtzin, mtzout, argsin)
    )
    log.flush()
    proc1 = subprocess.Popen(
        "freerflag hklin {0} hklout {1} << eof {2}".format(mtzin, mtzout, argsin),
        universal_newlines=True,
        stdout=log,
        shell=True,
    )
    print("--Running freerflag--")
    proc1.wait()
    log.flush()
    log.close()
    print("--Done--")
    log.close()

def check_grid_shape(mapin=None):
    m = gemmi.read_ccp4_map(mapin)
    g = m.grid
    g_shape = [m.header_i32(x) for x in (1,2,3)]
    if len(set(g_shape)) > 1:
        return True
    else:
        return False



if __name__ == "__main__":
    """
    Usage python scriptname EMDid(title) reso cycles sequence_file_path map_path sharpfact mode refine_mode
    e.g. ccp4-python run_bucref_ccpem_pipeline.py EMDXXXX 3.2 5 path/seqfile.fasta path/mapin.map -20.0 'EM' 'Global'

    This script will create a directory named *EMDid* in the current
    working directory, the CWD can be changed by reassigning a path
    to *job_location* in script.

    Arguments:
    jobdir   : Job folder to be created
    Resolution : Map resolution
    Cycles  : Number of pipeline (build + refine) cycles to run
    Sequence_file_path : Path and filename of sequence file
    Map_path    : Path and filename of input EM map
    sharpfact : sharpening factor to apply, >0 bluring, <0 sharpening, 0 is none
    mode : 'EM' default.
    refine_mode : default is 'Global'. set to 'Local' for local refinement
    """
    parser = argparse.ArgumentParser(
        description=("run bucaneer pipeline with servalcat")
    )
    parser.add_argument("--jobdir", help="Job folder to be created, where job will run")
    parser.add_argument("--reso", help="resolution of data", type=float)
    parser.add_argument(
        "--cycles", help="number of pipeline (build + refine) cycles to run", type=int
    )
    parser.add_argument("--seq", help="path and filename of sequence file")
    parser.add_argument("--map", help="path and filename of final EM map, if available")
    parser.add_argument("--hm1", help="path and filename of Halfmap1")
    parser.add_argument("--hm2", help="path and filename of halfmap2")
    parser.add_argument(
        "--mask",
        help="path and filename of mask, if available, if not given script will use EMDA to produce one from --map",
    )
    parser.add_argument(
        "--build_from_map",
        help="build from map given rather than nemap calculated from halfmaps",
        action="store_true",
    )
    parser.add_argument("--shifts", help="path and filename to trim_shifts.json")
    parser.add_argument("--sigf", help="intended ratio of F , default=0.1 (10 percent of F)", type=float, default=0.1)
    parser.add_argument("--continue_run", 
        help="continue previous job from cycle given, job will start buccaneer from given cycle number",
        type=int,
        default=0,
    )
    # if not len(sys.argv) >= 8:
    #    helpout = """
    """Usage:
    ccp4-python run_bucref_ccpem_pipeline.py EMDid resolution cycles path/seqfile path/mapin sharpfact mode refine_mode

    e.g. ccp4-python run_bucref_ccpem_pipeline.py EMDXXXX 3.2 5 path/seqfile.fasta path/mapin.map -20.0 'EM' 'Global'
        
    This script will create a directory named *EMDid* in the current
    working directory, the CWD can be changed by reassigning a path
    to *job_location* in script.

    Arguments:
    jobdir   : Job folder to be created
    Resolution : Map resolution
    Cycles  : Number of pipeline (build + refine) cycles to run
    Sequence_file_path : Path and filename of sequence file
    Map_path    : Path and filename of input EM map
    Halfmap1    : Path and filename of Halfmap 1
    Halfmap2    : Path and filename of Halfmap 2
    Mask        : Path and filename of Mask (optional) if not given script 
                  will use EMDA to produce one
    """
    #    print(helpout)
    # else:
    args = parser.parse_args()
    job_location = os.getcwd()
    jobdir = args.jobdir  # sys.argv[1]
    resolution = args.reso  # float(sys.argv[2])
    cycles = args.cycles  # int(sys.argv[3])
    seqin = args.seq  # sys.argv[4]
    mapin = args.map  # sys.argv[5]
    hm1 = args.hm1  # sys.argv[6]
    hm2 = args.hm2  # sys.argv[7]
    cont_cyc = args.continue_run
    if args.mask:
        mask = args.mask
    else:
        mask = False
    if args.shifts:
        shifts = args.shifts
    else:
        shifts = False
    #    if len(sys.argv) == 9:
    #         mask = sys.argv[8]
    #    else:
    #        mask = False
    # sharpfact = float(sys.argv[6])
    # mode = sys.argv[9]
    # refine_mode = sys.argv[9]
    # dirpath = os.path.join(job_location, EMD)
    # if sharpfact >= 0:
    #    if sharpfact == 0:
    #        col_suf='0'
    #        sharpdir = 'S0'
    #    else:
    #        col_suf='Blur_{0:.2f}'.format(sharpfact)
    #        sharpdir = 'B{0:.0f}'.format(sharpfact)
    # else:
    #    col_suf='Sharp_{0:.2f}'.format(-1*sharpfact)
    #    sharpdir = 'S{0:.0f}'.format(-1*sharpfact)
    if not os.path.exists(seqin):
        print("Cannot find sequence file. {0} does not exist".format(seqin))
        exit(1)
    if hm1 and hm2:
        if not os.path.exists(hm1):
            print("Cannot find Map file. {0} does not exist".format(hm1))
            exit(1)
        if not os.path.exists(hm2):
            print("Cannot find Map file. {0} does not exist".format(hm2))
            exit(1)
    workpath = os.path.join(job_location, jobdir)

    if not os.path.exists(workpath):
        os.mkdir(jobdir)
        os.chdir(workpath)
        # os.mkdir(sharpdir)
        # os.chdir(sharpdir)
    else:
        os.chdir(workpath)
        # if not os.path.exists(sharpdir):
        #    os.mkdir(sharpdir)
        # os.chdir(sharpdir)
    # make mtz from map
    noncubic = check_grid_shape(mapin=mapin)
    if cont_cyc == 0:
        if not mask:
            if hm1:
                mapmask(mapin=hm1)
                mask = "mapmask.mrc"
                trim_maps(mapin=mapin, hm1=hm1, hm2=hm2, mask=mask, noncubic=noncubic)
            else:
                mapmask(mapin=mapin)
                mask = "mapmask.mrc"
                trim_maps(mapin=mapin, mask=mask, noncubic=noncubic)
        else:
            if hm1:
                trim_maps(mapin=mapin, hm1=hm1, hm2=hm2, mask=mask, noncubic=noncubic)
            else:
                trim_maps(mapin=mapin, mask=mask, noncubic=noncubic)

    if hm1:
        hm1_trim = f"{os.path.basename(hm1).split('.')[0]}_trimmed.mrc"
        hm2_trim = f"{os.path.basename(hm2).split('.')[0]}_trimmed.mrc"
    else:
        hm1_trim = None
        hm2_trim = None
    if args.map:
        mapin_trim = f"{os.path.basename(mapin).split('.')[0]}_trimmed.mrc"
    mask_trim = f"{os.path.basename(mask).split('.')[0]}_trimmed.mrc"

    if cont_cyc == 0:
        if not args.build_from_map:
            run_halfmaptomtz(
                halfmaps=[hm1_trim, hm2_trim],
                mask=mask_trim,
                resolution=resolution,
            )
            #run_sftools(FOM=1, SIGF=args.sigf)
            set_sigf_fom_columns(SIGF=args.sigf)
            run_freerflag(mtzout="starting_map_buc.mtz")
            bucmtz = os.path.join(workpath, "starting_map_buc.mtz")
        else:
            run_maptomtz(mapin=mapin_trim, mask=mask_trim, resolution=resolution)
            if os.path.exists("masked_fs_obs.mtz"):
                #run_sftools(FOM=1, SIGF=args.sigf, mtzin="masked_fs_obs.mtz", mtzout="starting_map1.mtz")
                set_sigf_fom_columns(SIGF=args.sigf, mtzin="masked_fs_obs.mtz", mtzout="starting_map1.mtz")
            else:
                #run_sftools(FOM=1, SIGF=args.sigf, mtzin="starting_map.mtz", mtzout="starting_map1.mtz")
                set_sigf_fom_columns(SIGF=args.sigf, mtzin="starting_map.mtz", mtzout="starting_map1.mtz")
            run_freerflag(mtzin="starting_map1.mtz", mtzout="starting_map_buc.mtz")
            bucmtz = os.path.join(workpath, "starting_map_buc.mtz")
        refout = None
        # mtzin = bucmtz
        logfile = "bucpiperun_stdout.txt"
        print(
            "Build refine job : Running in {0} - {1}".format(
                jobdir, datetime.datetime.now()
            )
        )
        start_cyc = 1
    else:
        bucmtz = os.path.join(workpath, "starting_map_buc.mtz")
        refout = "refined{0}.pdb".format(str(cont_cyc-1))
        logfile = f"bucpiperun_stdout_from_cyc{cont_cyc}.txt"
        print(
            "Build refine job : Continue from {0}\nRunning in {1} - {2}".format(
                cont_cyc, jobdir, datetime.datetime.now())
        )
        start_cyc = cont_cyc
        
    sys.stdout.flush()
    CWD = os.getcwd()
    for i in range(start_cyc, cycles + 1):
        pdbout = run_buc(
            refout,
            bucmtz,
            seqin,
            resolution,
            i,
            jobdir,
            logfile,
            3,
            2,
            "EM",
            build_from_map=args.build_from_map,
        )
        if hm1:
            refout = run_servalcat(
                pdbout,
                resolution,
                10,
                i,
                logfile,
                mapin=None,
                hm1=hm1_trim,
                hm2=hm2_trim,
                #mode="Global",
                mode="Masked",
                mask=mask_trim,
                #mask_radius=3,
                symmetry_auto=None,
                set_bfactor=True,
                atom_bfactor=40,
                weight_auto=True,
                weight=0.0001,
                libg_restraints=None,
                jellybody_on=True,
                jellybody_sigma=0.01,
                jellybody_dmax=4.2,
                output_prefix=f"refined{i}",
            )
        else:
            refout = run_servalcat(
                pdbout,
                resolution,
                10,
                i,
                logfile,
                mapin=mapin_trim,
                hm1=hm1_trim,
                hm2=hm2_trim,
                #mode="Global",
                mode="Masked",
                mask=mask_trim,
                #mask_radius=3,
                symmetry_auto=None,
                set_bfactor=True,
                atom_bfactor=40,
                weight_auto=True,
                weight=0.0001,
                libg_restraints=None,
                jellybody_on=True,
                jellybody_sigma=0.01,
                jellybody_dmax=4.2,
                output_prefix=f"refined{i}",
            )
        os.rename("servalcat.log", f"servalcat{i}.log")
    print(
        "Build refine job : Finished in {0} - {1}".format(
            jobdir, datetime.datetime.now()
        )
    )
    sys.stdout.flush()
    os.chdir(job_location)
