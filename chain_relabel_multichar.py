# Stripped out code from cbuccaneer to relabel chain IDs
# Requires clipper to run
# Author: S.W.Hoh, University of York, 2019

import argparse
import clipper
import numpy as np

# chains labels = labels[0] and labels[1]
labels = [("A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
          "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
          "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d",
          "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
          "o", "p", "q", "r", "s", "t", "u", "v", "w", "x",
          "y", "z"), 
          ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")]


def split_chains_at_gap(mol,verbose=0):
    '''
    split chains at gap
    '''
    target = clipper.MiniMol(mol.spacegroup(), mol.cell())
    mp = clipper.MPolymer()
    #id_index=1
    for chn in range(0,mol.size()):
        # for checking set verbose value larger than 5
        if verbose > 5:
            print('chain id : {0}'.format(mol[chn].id()))

        mp = clipper.MPolymer()
        # splitting chains at gap
        for res in range(0,mol[chn].size()):
            mp.insert( mol[chn][res] )
            if res < (mol[chn].size() - 1):
                if not clipper.MMonomer_protein_peptide_bond(mol[chn][res],mol[chn][res+1]):
                    mp.set_id("")
                    target.insert( mp )
                    #id_index+=1
                    mp = clipper.MPolymer()
        if mp.size() > 0:
            mp.set_id("")
            #id_index+=1
            target.insert( mp )
    # for checking set verbose value larger than 5
    if verbose > 5:  
        print('target size {0}'.format(target.size()))
        count=0
        for p in target:
            print('chain id : {0}, chain size : {1}'.format(p.id(), p.size()))
            count += p.size()
        print('counted res {0}'.format(count))

    return target

def get_used_labels(chainid):
    row = -1
    col = -1
    isalp = True
    # check chars in chain id
    if len(chainid) == 1:
        row = 0
        if chainid.isalpha():
            col = labels[0].index(chainid)
        else:
            col = labels[1].index(chainid)
            isalp = False

    if len(chainid) == 2:
        # +1 to offset first row of single char
        if chainid.isalpha():
            row = labels[0].index(chainid[0]) + 1
            col = labels[0].index(chainid[1])
        else:
            row = labels[1].index(chainid[0])
            col = labels[1].index(chainid[1])
            isalp = False

    return (row,col,isalp)

def chain_label( mol ):
    # set up boolean numpy for each char combination
    lbl_alpha = np.full((53, 52), False, dtype=bool)
    lbl_num = np.full((10,10), False, dtype=bool)
    
    # get existing labels and mark their existence
    for chn in range(0, mol.size()):
        chnid = str(mol[chn].id())
        indices = get_used_labels(chnid)
        if indices[2]:
            lbl_alpha[indices[0]][indices[1]] = True
        else:
            lbl_num[indices[0]][indices[1]] = True

    # label chains
    lbl = 0
    for chn in range(0, mol.size()):
        if mol[chn].id() == "":
            newlabelled = False
        while True:
            newlabelled = False
            # label chains with letters
            if lbl < len(labels[0]):
                if not lbl_alpha[0][lbl]:
                    mol[chn].set_id(labels[0][lbl])
                    newlabelled = True
                    lbl += 1
                else:
                    lbl += 1
            else:
                if lbl < 2756:
                    c = ( lbl - len(labels[0]) ) % len(labels[0] )
                    r = ( lbl - len(labels[0]) ) / len(labels[0] )
                    if not lbl_alpha[r][c]:
                        newid = "{0}{1}".format(labels[0][r], labels[0][c])
                        mol[chn].set_id( newid )
                        newlabelled = True
                        lbl += 1
                    else:
                        lbl += 1
                else:
                    if lbl < 2856:
                        c = ( lbl - 2756 ) % len(labels[1])
                        r = ( lbl - 2756 ) / len(labels[1])
                    else:
                        c = ( lbl - 2756 ) % len(labels[1])
                        r = ( lbl - 2756 ) / len(labels[1]) % len(labels[1])
                    if not lbl_num[r][c]:
                        if r == 0:
                            newid = labels[1][c]
                        else:
                            newid = "{0}{1}".format(labels[1][r],labels[1][c])
                        rmax = 1
                        for f in range(0, mol.size()):
                            if mol[f].id() == newid:
                                rmax = max(rmax, mol[f][mol[f].size()-1].seqnum() + 5)
                        mol[chn].set_id( newid )
                        for res in range(0,mol[chn].size()):
                            mol[chn][res].set_seqnum( rmax + res )
                        newlabelled = True
                        lbl += 1                  
                    lbl += 1
            if newlabelled:
                break

    return mol

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=(
    "Re-label the chains in pdb file (re-coded from Buccaneer), "
    "this will output multi-character (2-char) label IDs for chains."
    "Run this with ccp4-python."
    "ccp4-python scriptname pdbinfile pdboutfile."))
    parser.add_argument("pdbin", help="PDB input file")
    parser.add_argument("pdbout", help="PDB output file")
    #uncomment the line below to write out file in mmcif format
    #parser.add_argument("cifout", help="mmCIF output file")
    args = parser.parse_args()

    mol1 = clipper.MiniMol()
    mfile = clipper.MMDBfile()
    mfile.read_file(args.pdbin)
    mfile.import_minimol(mol1)
    
      
    mol2 = split_chains_at_gap(mol1)
    mol3 = chain_label(mol2)

    mfile2 = clipper.MMDBfile()
    mfile2.export_minimol( mol3 )
    #in pdb format
    mfile2.write_file( args.pdbout, clipper.MMDBManager.PDB )
    #uncomment the line below to write out file in mmcif format
    #mfile2.write_file( args.cifout, clipper.MMDBManager.CIF )