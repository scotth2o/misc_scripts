# A standalone python script that runs the automated model building pipeline for EM
# same as the one in CCPEM package
# Requires CCP4 package to be installed to run (Buccaneer, REFMAC, sftools, freerflag)
# Author: S.W. Hoh, University of York, 2020
# Reference: 
# 'Current approaches for automated model building into cryo-EM maps using
# Buccaneer with CCP-EM', Soon Wen Hoh, Tom Burnley and Kevin Cowtan
# Acta Cryst. (2020). D76, 531-541, https://doi.org/10.1107/S2059798320005513


import os
import subprocess
import sys
import datetime


# Build with Buccaneer
def run_buc(pdbin, mtzfile, seqin, resolution, cycle, EMD, logfile, col_suf, int_cyc=5,cpujobs=2, mode='EM',keyfile=None, filesuffix=''):
    '''
    Setup and run Buccanner for the cycle
    Arguments:
    pdbin   : PDB file to continue from, set as None if starting without one
    mtzfile : Reflections data file (produced using maptomtz function that runs REFMAC)
    seqin   : Structure sequence file
    resolution : Resolution of the map
    cycle   : Current pipeline cycle count
    EMD     : Just a name of the EM data to set to title
    logfile : Path for stdout logfile
    col_suf : Column suffix, used when sharpening or blurring applied, and S0 for none
    int_cyc : internal Buccaneer cycle, default=5
    cpujobs : default set to 2
    mode    : set to 'EM' to get the EM reference datafile, otherwise xtal
    keyfile : file with keywords, separate each one with newline (optional)
    filesuffix : suffix for output file (optional)

    Returns: Path to Buccaneer output PDB file
    '''
    CWD=os.getcwd()

    if not os.path.isfile(seqin):
        print('Sequence file {0} not found.'.format(seqin))
        return
    if not os.path.isfile(mtzfile):
        print('mtzfile not found at {0}.'.format(mtzfile))
        return
    pdbout = 'build{0}{1}.pdb'.format(str(cycle),str(filesuffix))
    xmlout = 'program{0}{1}.xml'.format(str(cycle),str(filesuffix))

    if pdbin == None:
        pdbin_line = ''
    else:
        pdbin_line = 'pdbin {0}'.format(pdbin)

    if keyfile == None:
        keywords_line = ''
    else:
        fopen = open(keyfile, 'r')
        keywords_line = ''
        for line in fopen:
            keywords_line += line
    #clibd + em reference path
    if mode == 'EM':
        pdbin_ref = os.path.join(os.environ['CLIBD'], 'reference_structures/reference-EMD-4116.pdb')
        mtzin_ref = os.path.join(os.environ['CLIBD'], 'reference_structures/reference-EMD-4116.mtz')
    else:
        pdbin_ref = os.path.join(os.environ['CLIBD'], 'reference_structures/reference-1tqw.pdb')
        mtzin_ref = os.path.join(os.environ['CLIBD'], 'reference_structures/reference-1tqw.mtz')


    argsin = '''
title {0}_piperun
pdbin-ref {1}
mtzin-ref {2}
colin-ref-fo FP.F_sigF.F,FP.F_sigF.sigF
colin-ref-hl FC.ABCD.A,FC.ABCD.B,FC.ABCD.C,FC.ABCD.D
seqin {3}
mtzin {4}
colin-fo Fout{5},SIGFem
colin-free FreeR_flag
colin-phifom Pout0,FOMem
cycles {6}
jobs {7}
anisotropy-correction
fast
correlation-mode
resolution {8}
pdbout {9}
xmlout {10}
{11}
{12}
eof
'''.format(EMD,
            pdbin_ref,
            mtzin_ref,
            seqin,
            mtzfile,
            col_suf,
            int_cyc,
            cpujobs,
            resolution,
            pdbout,
            xmlout,
            pdbin_line,
            keywords_line)

    log = open(logfile,'a')
    log.writelines('cbuccaneer --stdin << eof {0}'.format(argsin))
    log.flush()
    proc = subprocess.Popen('cbuccaneer --stdin << eof {0}'.format(argsin),
                            universal_newlines=True, stdout=log, shell=True)
    print('Running Buccaneer cycle {0}'.format(cycle))
    sys.stdout.flush()
    proc.wait()
    log.flush()
    log.close()
    return pdbout

# Refinement with REFMAC
def run_ref(pdbin,bucmtz,resolution,cycle,logfile,sharpfact,ref_cyc=20,mode='Global',libgref=None):
    '''
    Setup and runs refinement with REFMAC in between Buccanner runs
    Arguments:
    pdbin   : PDB input
    bucmtz  : Initial reflections data file (in CCPEM pipeline, the initial reflections calculated from original map is used)
    Resolution : Resolution of the map
    cycle   : Current pipeline cycle count
    logfile : Path of stdout logfile
    sharpfact : sharpening factor >0 blur, <0 sharp, 0 is none
    ref_cyc : Interal REFMAC refinement cycles, default is 20
    mode    : Default is 'Global' for global refinement, 'Local' for local refinement
    libgref : libg reference file produced using ligb 

    Returns : Path to refined PDB output
    '''

    # local refinement
    if mode == 'Local':
        hklin = 'masked_fs.mtz'
        shiftline = '@shifts.txt'
    else: # global refinement (default)
        hklin = bucmtz
        shiftline = ''
    hklout = 'refine.mtz'
    xyzout = 'refined{0}.pdb'.format(str(cycle))
    if sharpfact >= 0:
       if sharpfact == 0:
           ref_line = ''
       else:
           ref_line = 'refine sharp {0:.2f}'.format(-1*sharpfact)
    else:
       ref_line = 'refine sharp {0:.2f}'.format(-1*sharpfact)

    if libgref != None:
       libgresline = '@{0}'.format(libgref)
    else:
       libgresline = ''
    argsin = '''
make hydr no
solvent no
source EM MB
ncycle {0}
weight auto
reso {1}
ridge dist sigma 0.01
ridge dist dmax 4.2
labin PHIB=Pout0 FP=Fout0
PHOUT
{2}
{3}
{4}
end
eof
'''.format(ref_cyc, resolution,
           ref_line,
           shiftline,
           libgresline)

    log = open(logfile, 'a')
    log.writelines('\n----------------------\n')
    log.writelines('refmac5 xyzin {0} hklin {1} xyzout {2} hklout {3} << eof {4}'.format(
                    pdbin,hklin,xyzout,hklout,argsin))
    log.flush()
    proc = subprocess.Popen('refmac5 xyzin {0} hklin {1} xyzout {2} hklout {3} << eof {4}'.format(
                            pdbin,hklin,xyzout,hklout,argsin),
                            universal_newlines=True, stdout=log, shell=True)
    if mode == 'Local':
        print('Running Refmac local refine cycle {0}'.format(cycle))
    else:
        print('Running Refmac cycle {0}'.format(cycle))
    sys.stdout.flush()
    proc.wait()
    log.flush()
    log.close()
    return xyzout

# Set files for local refinement
def local_maptomtz(logfile,cycle,resolution=2.0, jobloc=None, mappath=None, pdbpath=None, sharpfact=None):
    '''
    Setup and run REFMAC to get shifted/'reboxed' PDB file for local refinement
    Arguments:
    logfile : Path to stdout logfile
    cycle   : Current pipeline cycle count
    resolution : Resolution of map
    jobloc  : Working directory
    mappath : Path to original input map
    pdbpath : PDB file produced from Buccaneer
    sharpfact : Sharpening factor, >0 blur, <0 sharp, 0 is none

    Returns : 'Reboxed' PDB 
    '''
    hklout = 'local_startmap.mtz'
    xyzout = 'shifted_local.pdb'
    if sharpfact >= 0:
       if sharpfact == 0:
           ref_line = 'sfcalc sharp 0.0'
       else:
           ref_line = 'sfcalc blur {0:.2f}'.format(sharpfact)
    else:
       ref_line = 'sfcalc sharp {0:.2f}'.format(-1*sharpfact)
    argsin='''
mode sfcalc
source EM MB
reso {0}
sfcalc mrad 3.0
sfcalc shift
{1}
end
eof
'''.format(resolution,
              ref_line)

    log = open(logfile, 'a')
    log.writelines('\n----------------------\n')
    log.writelines('refmac5 mapin {0} hklout {1} xyzin {2} xyzout {3} << eof {4}'.format(
                    mappath,hklout,pdbpath,xyzout,argsin))
    log.flush()
    proc = subprocess.Popen('refmac5 mapin {0} hklout {1} xyzin {2} xyzout {3} << eof {4}'.format(
                            mappath,hklout,pdbpath,xyzout,argsin),
                            universal_newlines=True, stdout=log, shell=True)
    print('Running map to mtz (local) {0}'.format(cycle))
    sys.stdout.flush()
    proc.wait()
    log.flush()
    log.close()
    return xyzout

# Make mtz file from map
def run_maptomtz(mapin=None, resolution=2.0,  sharpfact=0):
    '''
    Use REFMAC to create mtz file
    Arguments:
    mapin : Input map
    resolution : map resolution
    textin : sharpening and blurring factor, 0 for none.
             e.g. 0,50,100 will apply sharpening and blurring factor of 50 and 100 respectively.

    Output : mtz file
    '''
    hklout = 'starting_map.mtz'
    textin = ''
    if sharpfact >= 0:
        if sharpfact == 0:
            textin = 'sharp 0' #none
        else:
            textin = 'blur 0, {0}'.format(sharpfact) #blurring
    else:
        textin = 'sharp 0, {0}'.format(-1*sharpfact) #sharpening

    argsin = '''
mode sfcalc
source EM MB
reso {0}
sfcalc {1}
end
eof
'''.format(resolution, textin)

    logfile = 'maptomtz_stdout.txt'
    log = open(logfile,'w')
    log.writelines('refmac5 mapin {0} hklout {1} << eof {2}'.format(mapin, hklout, argsin))
    log.flush()
    proc = subprocess.Popen('refmac5 mapin {0} hklout {1} << eof {2}'.format(mapin, hklout, argsin),
                            universal_newlines=True, stdout=log, shell=True)
    print('Generating mtz from {0}'.format(mapin))

    proc.wait()
    log.flush()
    log.close()
    print('--Done--')


# sftools
def run_sftools(FOM=1.0):
    '''
    Create 'fake' columns SIGF and FOM
    '''
    argsin='''
READ starting_map.mtz
SET SPACEGROUP
P 1
CALC Q COL SIGFem = 1
CALC W COL FOMem = {0}
WRITE sftools_out.mtz
END
eof
'''.format(FOM)

    logfile = 'sftools_stdout.txt'
    log = open(logfile,'w')
    log.writelines('sftools << eof {0}'.format(argsin))
    log.flush()
    proc = subprocess.Popen('sftools << eof {0}'.format(argsin),
                            universal_newlines=True, stdout=log, shell=True)
    print('--Running sftools--')
    proc.wait()
    log.flush()
    log.close()
    print('--Done--') 

# run freerflag
def run_freerflag(mtzin='sftools_out.mtz', mtzout='buccaneer_fom1.mtz'):
    argsin='''
FREERFRAC 0.05
eof
'''
    logfile = 'setfreerflag_stdout.txt'
    log = open(logfile,'w')
    log.writelines('freerflag hklin {0} hklout {1} << eof {2}'.format(mtzin,mtzout,argsin))
    log.flush()
    proc1 = subprocess.Popen('freerflag hklin {0} hklout {1} << eof {2}'.format(mtzin,mtzout,argsin),
                             universal_newlines=True, stdout=log, shell=True)
    print('--Running freerflag--')
    proc1.wait()
    log.flush()
    log.close()
    print('--Done--')
    log.close()



if __name__ == '__main__':
    '''
    Usage python scriptname EMDid(title) reso cycles sequence_file_path map_path sharpfact mode refine_mode
    e.g. ccp4-python run_bucref_ccpem_pipeline.py EMDXXXX 3.2 5 path/seqfile.fasta path/mapin.map -20.0 'EM' 'Global'

    This script will create a directory named *EMDid* in the current
    working directory, the CWD can be changed by reassigning a path
    to *job_location* in script.

    Arguments:
    EMDid   : Job folder to be created
    Resolution : Map resolution
    Cycles  : Number of pipeline (build + refine) cycles to run
    Sequence_file_path : Path and filename of sequence file
    Map_path    : Path and filename of input EM map
    sharpfact : sharpening factor to apply, >0 bluring, <0 sharpening, 0 is none
    mode : 'EM' default.
    refine_mode : default is 'Global'. set to 'Local' for local refinement
    '''
    if len(sys.argv) != 9:
        helpout = '''
    Usage:
    ccp4-python run_bucref_ccpem_pipeline.py EMDid resolution cycles path/seqfile path/mapin sharpfact mode refine_mode

    e.g. ccp4-python run_bucref_ccpem_pipeline.py EMDXXXX 3.2 5 path/seqfile.fasta path/mapin.map -20.0 'EM' 'Global'
        
    This script will create a directory named *EMDid* in the current
    working directory, the CWD can be changed by reassigning a path
    to *job_location* in script.

    Arguments:
    EMDid   : Job folder to be created
    Resolution : Map resolution
    Cycles  : Number of pipeline (build + refine) cycles to run
    Sequence_file_path : Path and filename of sequence file
    Map_path    : Path and filename of input EM map
    sharpfact : sharpening factor to apply, >0 bluring, <0 sharpening, 0 is none
    mode : 'EM' default.
    refine_mode : default is 'Global'. set to 'Local' for local refinement
    '''
        print(helpout)
    else:
        job_location = os.getcwd()
        EMD = sys.argv[1]
        resolution = float(sys.argv[2])
        cycles = int(sys.argv[3])
        seqin = sys.argv[4]
        mapin = sys.argv[5]
        sharpfact = float(sys.argv[6])
        mode = sys.argv[7]
        refine_mode = sys.argv[8]
        if refine_mode == 'local':
            refine_mode = 'Local'
        if refine_mode == 'global':
            refine_mode = 'Global'
        #dirpath = os.path.join(job_location, EMD)
        
        if sharpfact >= 0:
            if sharpfact == 0:
                col_suf='0'
                sharpdir = 'S0'
            else:
                col_suf='Blur_{0:.2f}'.format(sharpfact)
                sharpdir = 'B{0:.0f}'.format(sharpfact)
        else:
            col_suf='Sharp_{0:.2f}'.format(-1*sharpfact)
            sharpdir = 'S{0:.0f}'.format(-1*sharpfact)

        if not os.path.exists(seqin):
            print('Cannot find sequence file. {0} does not exist'.format(seqin))
            exit(1)

        if not os.path.exists(mapin):
            print('Cannot find Map file. {0} does not exist'.format(seqin))
            exit(1)

        workpath = os.path.join(job_location, EMD)
        if not os.path.exists(workpath):
            os.mkdir(EMD)
            os.chdir(workpath)
            os.mkdir(sharpdir)
            os.chdir(sharpdir)
        else:
            os.chdir(workpath)
            if not os.path.exists(sharpdir):
                os.mkdir(sharpdir)
            os.chdir(sharpdir)
        
        #make mtz from map
        run_maptomtz(mapin, resolution, sharpfact)
        run_sftools(FOM=1)
        run_freerflag()

        bucmtz = os.path.join(workpath,'{0}/buccaneer_fom1.mtz'.format(sharpdir))
        refout = None
        #mtzin = bucmtz
        logfile = 'bucpiperun_stdout.txt'
        print('Build refine job : Running in {0} - {1}'.format(EMD,datetime.datetime.now()))
        sys.stdout.flush()
        CWD=os.getcwd()
        for i in range(1,cycles+1):
            pdbout = run_buc(refout,bucmtz,seqin,resolution,i,EMD,logfile,col_suf,5,2,mode)
            if refine_mode == 'Local':
                pdbout = local_maptomtz(logfile,i,resolution, CWD, mapin, pdbout, sharpfact)
            refout = run_ref(pdbout,bucmtz,resolution,i,logfile,sharpfact,20,refine_mode)

        print('Build refine job : Finished in {0} - {1}'.format(EMD,datetime.datetime.now()))
        sys.stdout.flush()
        os.chdir(job_location)
